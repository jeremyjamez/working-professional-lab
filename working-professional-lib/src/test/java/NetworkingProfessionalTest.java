import models.*;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NetworkingProfessionalTest {

    @Test
    public void shouldFixRouter(){
        Professional professional = new NetworkingProfessional("John");
        Router router = new CiscoRouter("Linksys", 100f, 1);
        Tool tool = new Pliers("Pliers", "Yellow");
        router.updateDamage(5);
        assertTrue(((NetworkingProfessional)professional).fixRouter(router, tool));
    }
}

import interfaces.IPlumbingProfessional;
import models.Pipe;
import models.PvcPipe;
import models.Tool;
import models.Wrench;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PlumbingProfessionalTest {

    @Test
    public void shouldFixPipe(){
        IPlumbingProfessional professional = new PlumbingProfessional("Jack");
        Pipe pipe = new PvcPipe("White");
        Tool tool = new Wrench("Wrench", 10f);
        pipe.updateDamage(5);
        assertTrue(professional.fixPipe(pipe, tool));
    }
}

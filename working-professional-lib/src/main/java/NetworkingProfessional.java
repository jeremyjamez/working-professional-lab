import interfaces.INetworkingProfessional;
import models.Professional;
import models.Router;
import models.Tool;

public class NetworkingProfessional extends Professional implements INetworkingProfessional {

    public NetworkingProfessional(String name) {
        super(name);
    }

    public boolean fixRouter(Router router, Tool tool) {
        if (router.getDamage() > 0){
            System.out.println(tool.getBrand() + " is being used to fix the " + router.getBrand() + " Router");
            router.updateDamage(0);
            return true;
        }
        return false;
    }
}

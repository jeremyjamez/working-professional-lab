package interfaces;

import models.Router;
import models.Tool;

public interface INetworkingProfessional {
    boolean fixRouter(Router router, Tool tool);
}

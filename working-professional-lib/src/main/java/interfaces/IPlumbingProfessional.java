package interfaces;

import models.Pipe;
import models.Tool;

public interface IPlumbingProfessional {
    boolean fixPipe(Pipe pipe, Tool tool);
}

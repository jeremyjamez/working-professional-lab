package interfaces;

public interface BreakableItem {
    void updateDamage(float damage);
}

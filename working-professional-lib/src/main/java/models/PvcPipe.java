package models;

public class PvcPipe extends Pipe {

    private String colour;

    public PvcPipe() {
    }

    public PvcPipe(String colour) {
        this.colour = colour;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void updateDamage(float damage) {
        this.damage = damage;
    }
}

package models;

public final class CiscoRouter extends Router {

    private int ciscoCode;

    public CiscoRouter() {
    }

    public CiscoRouter(String brand, float bandwidth, int ciscoCode) {
        super(brand, bandwidth);
        this.ciscoCode = ciscoCode;
    }

    public int getCiscoCode() {
        return ciscoCode;
    }

    public void setCiscoCode(int ciscoCode) {
        this.ciscoCode = ciscoCode;
    }

    public void updateDamage(float damage) {
        this.damage = damage;
    }
}

package models;

import interfaces.BreakableItem;

public abstract class Router implements BreakableItem {

    private String brand;
    private float bandwidth;
    protected float damage;

    public Router() {
    }

    public Router(String brand, float bandwidth) {
        this.brand = brand;
        this.bandwidth = bandwidth;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public float getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(float bandwidth) {
        this.bandwidth = bandwidth;
    }

    public float getDamage() {
        return damage;
    }
}

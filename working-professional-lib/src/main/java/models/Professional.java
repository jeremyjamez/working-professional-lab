package models;

import models.Certification;

import java.util.ArrayList;
import java.util.List;

public abstract class Professional {
    private int id;
    private String name;
    private float salary;
    private List<Certification> certifications = new ArrayList<Certification>();

    public Professional(String name) {
        this.name = name;
    }

    public void addCertification(Certification certification){
        certifications.add(certification);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public List<Certification> getCertifications() {
        return certifications;
    }

    public void setCertifications(List<Certification> certifications) {
        this.certifications = certifications;
    }
}

package models;

import interfaces.BreakableItem;

public abstract class Pipe implements BreakableItem {

    protected float length;
    protected float diameter;
    protected float damage;

    public Pipe() {
    }

    public Pipe(float length, float diameter) {
        this.length = length;
        this.diameter = diameter;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getDiameter() {
        return diameter;
    }

    public void setDiameter(float diameter) {
        this.diameter = diameter;
    }

    public float getDamage() {
        return damage;
    }
}

package models;

import interfaces.BreakableItem;

public abstract class Tool {
    private String brand;

    public Tool(String brand){
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public boolean fix(BreakableItem item) {
        item.updateDamage(0);
        return true;
    }
}

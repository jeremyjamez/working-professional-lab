import interfaces.IPlumbingProfessional;
import models.Pipe;
import models.Professional;
import models.Tool;

public class PlumbingProfessional extends Professional implements IPlumbingProfessional {

    public PlumbingProfessional(String name) {
        super(name);
    }

    public boolean fixPipe(Pipe pipe, Tool tool) {
        if (pipe.getDamage() > 0){
            System.out.println(tool.getBrand() + " is being used to fix the " + pipe.getClass().getSimpleName());
            pipe.updateDamage(0);
            return true;
        }
        return false;
    }
}

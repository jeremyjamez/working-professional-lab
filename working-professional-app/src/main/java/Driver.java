import interfaces.BreakableItem;
import models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Driver {

    public static void main(String[] args){
        List<Tool> tools = new ArrayList<Tool>();
        Professional prof;
        Scanner input = new Scanner(System.in);
        String name;
        BreakableItem item;

        tools.add(new Wrench("Wrench", 10f));
        tools.add(new Pliers("Pliers", "Yellow"));
        tools.add(new Pliers("", "Black"));
        tools.add(new Wrench("", 12f));
        tools.add(new Wrench("", 14f));

        System.out.println("What is your profession?\n1. Plumbing Professional\n2. Networking Professional");
        int choice = input.nextInt();

        System.out.println("Enter your name");
        name = input.nextLine();
        input.next();

        if (choice == 1){
            prof = new PlumbingProfessional(name);
            item = new PvcPipe();
            item.updateDamage(5);
            if (((PlumbingProfessional)prof).fixPipe((Pipe) item, tools.get(0)))
                System.out.println("Amazing Work!");
        } else if(choice == 2){
            prof = new NetworkingProfessional(name);
            item = new CiscoRouter("Linksys", 100f, 1);
            item.updateDamage(5);
            if (((NetworkingProfessional)prof).fixRouter((Router) item, tools.get(1)))
                System.out.println("Amazing Work!");
        } else {
            System.out.println("Invalid input");
        }
    }
}
